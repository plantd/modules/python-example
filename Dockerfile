FROM registry.gitlab.com/plantd/modules/base:v1-alpine3.10
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

# Create the virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies
RUN pip install --upgrade pip
RUN apk update \
    && apk upgrade
    # && apk add \
        # add required packages here

# Add project
RUN mkdir /module
COPY . /module
WORKDIR /module
RUN pip install -r requirements.txt

# Put the overrides file in the venv
RUN cp /usr/lib/python3.7/site-packages/gi/overrides/Apex.py \
    /opt/venv/lib/python3.7/site-packages/gi/overrides/

ENV G_MESSAGES_DEBUG=module
ENV PLANTD_MODULE_ARGS=
ENV PLANTD_MODULE_ENDPOINT=tcp://localhost:5556
ENV PLANTD_MODULE_CONFIG_FILE=data/module.json

EXPOSE 5556
# Execute application
CMD python module.py $PLANTD_MODULE_ARGS
