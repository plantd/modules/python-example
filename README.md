# Python Example (minimal)

This is a toy `plantd` implementation that:

- runs as a standalone module with internal broker
- raises events on an associated PUB/SUB bus
- listens for events on the same PUB/SUB bus

It's suggested that you use `messenger` as the client to interact with this
simple example.

## ToDo

- [ ] add setup above
- [ ] finish docker example and test
- [ ] describe messenger install?


## Dependencies

These instructions require that libplantd has been installed on the system,
using an installation prefix of /usr.

```bash
git clone git@gitlab.com:plantd/libplantd.git
cd libplantd/
meson -Dprefix=/usr -Dshared-lib=true _build
ninja -C _build
```

If this is the first time building `libplantd` you will need to complete the
compilation before enabling examples (this is done above). Then continue and
install with examples:

```bash
meson configure -Denable-examples=true _build
ninja -C _build
```

The PUB/SUB component of this example needs a proxy type, an example of this can
be built from the `go-zapi` library, but since these instructions require
`libplantd` that's what we'll use here.

```bash
G_MESSAGES_DEBUG=all _build/examples/proxy
```

## Installation

The example can be run as a local service or within a Docker container. The
methods are outlined in the following sections.

### Local Install

Clone the repo and set up a virtual environment:

```
git clone https://gitlab.com/plantd/libplantd/modules/python-example
cd python-example
python3 -m venv .venv
```

You will need to copy the PyGObject overrides to the newly created virtual env:
```
cp /path/to/libplantd/apex/Apex.py \
  .venv/lib64/python3.7/site-packages/gi/overrides/
```

Now activate the virtual environment, install requirements and run:
```
source .venv/bin/activate
pip install -r requirements.txt
G_MESSAGES_DEBUG=all \
  PLANTD_MODULE_STANDALONE=true \
  PLANTD_MODULE_ENDPOINT="tcp://localhost:5556" \
  PLANTD_MODULE_BROKER="tcp://*:5556" \
  PLANTD_MODULE_CONFIG_FILE=data/module.json \
  PLANTD_MODULE_SERVICE=python-example \
  TEST_EVENTS_FRONTEND="tcp://localhost:9201" \
  TEST_EVENTS_BACKEND="tcp://localhost:9200" \
  ./module.py
```

You can test that this is listening with the command `ss -anp | grep 5556`.

## Docker

Running using docker-compose:

```bash
docker pull registry.gitlab.com/plantd/modules/python-example:v1
docker-compose up
```

If you have not built a local copy of the image (see below) it will pull the
most recent copy from the gitlab registry.

### Building

The current docker image to use as a base for modules is kept in the `libplantd`
registry.

```bash
docker pull registry.gitlab.com/plantd/modules/base:v1-alpine3.10
```

If you have already cloned the repository, you may skip this step:
```bash
git clone https://gitlab.com/plantd/modules/python-example
cd python-example
```

To build a new version of the image:
```bash
docker build -t registry.gitlab.com/plantd/modules/python-example:v1 .
```

## Testing

Once the service is running, either in a Docker container or on  your system, it
should be possible to send a test job to the example module, have it raise an
event that's received at the frontend of the proxy and delivered to the
backend, received by the module, and printed to the terminal.

To do this use "Submit Job" in `plantd-messenger` with the following fields:
- Endpoint: `tcp://localhost:5556`
- Service: `python-example`
- Request: Submit Job
  - ID: `` (leave blank)
  - Name: `test`
  - Value: `hello-world`

You should see a response in the terminal running the service saying:
```bash
python-example_1  | __main__-INFO: 00:24:16.295: submit-job: test [hello-world]
python-example_1  | (module.py:1): __main__-DEBUG: 00:24:16.297: Your jobValue = hello-world
python-example_1  | (module.py:1): __main__-DEBUG: 00:24:16.298: Sucessfully ran the test job!
```

