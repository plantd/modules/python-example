#!/usr/bin/env python

import signal
import sys
import os
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib, GObject

#pylint: enable=wrong-import-position

class App(Apex.Application):
    __gtype_name__ = "App"

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         application_id="org.plantd.state.Test",
                         flags=0,
                         **kwargs)
        # load environment
        service = os.getenv("PLANTD_MODULE_SERVICE", "state-test")
        endpoint = os.getenv("PLANTD_MODULE_ENDPOINT", "tcp://localhost:5555")
        source_endpoint = os.getenv("TEST_EVENTS_FRONTEND", "tcp://localhost:11005")
        # configure application
        self.set_endpoint(endpoint)
        self.set_service(service)
        self.set_inactivity_timeout(10000)
        # setup events
        self.event_source = Apex.Source.new(source_endpoint, "")
        self.add_source("event", self.event_source)
        self.event_sink = EventSink()
        # start things
        self.event_source.start()
        self.event_sink.start()

    # Here we can override the apex_application_submit_job
    def do_submit_job(self, job_name, job_value, job_properties):
        Apex.info("submit-job: %s [%s]" % (job_name, job_value))
        response = Apex.JobResponse.new()
        # for our "test" job just pass the job_value to the TestJob class
        if job_name == "test":
            job = TestJob(job_value)
            # job.connect("event", self.on_publish_event)
            event = Apex.Event.new_full(1000, "foo", "foo-bar-baz")
            self.send_event(event)
            response.set_job(job)
        else:
            pass
        return response


class TestJob(Apex.Job):
    __gtype_name__ = "TestJob"
    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, job_value, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.job_value = job_value

    def do_task(self):
        Apex.debug("Your jobValue = %s" % self.job_value)
        Apex.debug("Sucessfully ran the test job!")


class EventSink(Apex.Sink):
    __gtype_name__ = "EventSink"

    def __init__(self, *args, **kwargs):
        endpoint = os.getenv("TEST_EVENTS_BACKEND", "tcp://localhost:12000")
        super().__init__(*args,
                         **kwargs)
        self.set_endpoint(endpoint)
        self.set_filter("")

    def do_handle_message(self, msg):
        event = Apex.Event.new()
        #event.deserialize(msg)
        Apex.debug(msg)


def run_module():
    log_file = os.getenv("PLANTD_MODULE_LOG_FILE", "/dev/null")

    Apex.log_init(True, log_file)

    app = App()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    status = app.run(sys.argv)

    Apex.log_shutdown()

    return status

def main():
    return run_module()

if __name__ == '__main__':
    sys.exit(main())
